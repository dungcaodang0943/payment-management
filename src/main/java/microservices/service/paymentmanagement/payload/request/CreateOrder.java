package microservices.service.paymentmanagement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrder {
    @NotBlank
    private String typePayment;

    private long amount;

    @NotBlank
    @Size(max = 256)
    private String description;


    @NotBlank
    @Size(max = 2048)
    private String item;

    @NotBlank
    @Size(max = 1024)
    private String embed_data;

    private String codeOrders;
}
