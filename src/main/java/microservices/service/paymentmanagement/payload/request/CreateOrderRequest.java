package microservices.service.paymentmanagement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderRequest {
    @NotBlank
    private int app_id;

    @NotBlank
    @Size(max = 50)
    private String app_user;

    @NotBlank
    @Size(max = 40)
    private String app_trans_id;

    @NotBlank
    private long app_time;

    @NotBlank
    private long amount;

    private String key1;
    private String key2;

    private String order_type = "TRANSPORTATION";

    @Size(max = 256)
    private String title;

    @NotBlank
    @Size(max = 256)
    private String description;

    private String callback_url;

    @Size(max = 256)
    private String device_info;

    @NotBlank
    @Size(max = 2048)
    private String item;

    @NotBlank
    @Size(max = 1024)
    private String embed_data;

    private String currency;

    @NotBlank
    @Size(max = 20)
    private String bank_code;

    @Size(max = 50)
    private String phone;

    @Size(max = 100)
    private String email;

    @Size(max = 1024)
    private String address;
//
//    @Size(max = 50)
//    private String sub_app_id;
//
    private String redirect_url;
//
    private String more_param;

    @NotBlank
    private String mac;
}
