package microservices.service.paymentmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RedirectUrlRequest {
    private int appid;
    private String apptransid;
    private int pmcid;
    private String bankcode;
    private long amount;
    private long discountamount;
    private int status;
    private String checksum;
}
