package microservices.service.paymentmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CallbackCreateOrderRequest {
    private String data;
    private String mac;
    private String type;
}
