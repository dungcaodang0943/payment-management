package microservices.service.paymentmanagement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdatePamentStatusDataRequest {
    private long orderId;
    private int paymentStatus;
}
