package microservices.service.paymentmanagement.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleResponse {
    private Long id;

    private float totalWeight;

    private float currentWeight = 0;

    public VehicleResponse(Long id,float currentWeight, float totalWeight) {
        this.id = id;
        this.currentWeight = currentWeight;
        this.totalWeight = totalWeight;
    }
}
