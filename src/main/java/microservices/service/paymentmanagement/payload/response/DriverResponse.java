package microservices.service.paymentmanagement.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverResponse {
    private Long id;
    private String username;
    private String email;
    private String phone;
    private String address;
    private String token;
    private String type = "Bearer";
    private String role;
    int status;
    Object vehicleResponse;

    public DriverResponse(Long id, String username, String email, String phone, String address, String token, String role, int status, Object vehicleResponse) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.token = token;
        this.role = role;
        this.status = status;
        this.vehicleResponse = vehicleResponse;
    }
}
