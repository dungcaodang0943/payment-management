package microservices.service.paymentmanagement.payload.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtResponse {
    private Long id;
    private String username;
    private String email;
    private String phone;
    private String address;
    private String token;
    private String type = "Bearer";
    private String role;
    private String provinceCode;

    public JwtResponse(Long id, String username, String email, String phone, String address, String token, String role, String provinceCode) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.token = token;
        this.role = role;
        this.provinceCode = provinceCode;
    }
}
