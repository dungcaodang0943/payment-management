package microservices.service.paymentmanagement.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@ConfigurationProperties("authmanagement.app")
@Configuration
public class AppProperties {

    boolean authen = false;
    boolean verifySig = false;
    private String entranceDomain = "";
    private int httpSecondTimeout = 5;

    private WebConfig web = new WebConfig();

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public class WebConfig {
        private int clientID = 0;
        private String hashKey = "";
    }
}
