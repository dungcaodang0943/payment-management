package microservices.service.paymentmanagement.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@ConfigurationProperties("auth-management.kafka")
@Configuration
public class KafkaProperties {
    private String topic = "";
}
