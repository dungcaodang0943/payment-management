package microservices.service.paymentmanagement.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Mapper {

    public static final ObjectMapper mapper = new ObjectMapper();
}
