package microservices.service.paymentmanagement.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import microservices.service.paymentmanagement.ZaloPayUtil.vn.zalopay.crypto.HMACUtil;
import microservices.service.paymentmanagement.payload.request.*;
import microservices.service.paymentmanagement.payload.response.CallbackCreateOrderResponse;
import microservices.service.paymentmanagement.payload.response.CreateOrderResponse;
import microservices.service.paymentmanagement.payload.response.RedirectUrlResponse;
import microservices.service.paymentmanagement.utils.GsonUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Log4j2
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${payment-management.app.entranceDomain}")
public class MerchantServer {

    private int appId = 2553;
    private String key1 = "PcY4iZIKFCIdgZvA6ueMcMHHUbRLYjPL";
    private String key2 = "kLtgPl8HHhfvMuDHPwKfgfsY4Ydm9eIz";
    private List<Long> codeOrders;

    @Value("${payment-management.app.createOrderDomain}")
    private String createOrderDomain;

    @Value("${payment-management.app.callbackCreateOrderDomain}")
    private String callbackCreateOrderDomain;

    @Value("${payment-management.app.updatePaymentStatusDomain}")
    private String updatePaymentStatusDomain;

    @Value("${payment-management.app.redirectUrlDomain}")
    private String redirectUrlDomain;

    @PostMapping("create-order")
    public ResponseEntity<?> createOrder(@Valid @RequestBody CreateOrder createOrder) {
        DateFormat df = new SimpleDateFormat("yyMMdd");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Random ran = new Random();
        int uniqueId = ran.nextInt(1000000);
        String typePayment = createOrder.getTypePayment();
        long appTime = timestamp.getTime();
        long amount = createOrder.getAmount() / 1000;
        String appUser = "user123";
        String orderType = "TRANSPORTATION";
        String appTransId = df.format(new Date()) + "_" + uniqueId;
        String embedData = createOrder.getEmbed_data();
        String item = createOrder.getItem();
        String description = createOrder.getDescription();
        String bankCode = createOrder.getTypePayment();
        String orders = createOrder.getCodeOrders();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            codeOrders = objectMapper.readValue(orders, new TypeReference<List<Long>>(){});
        } catch (Exception ex) {
            log.info("Object Mapper: " + ex.getMessage());
        }
        log.info("Code orders: " + codeOrders);
        if (typePayment.equals("ATM")) {
            bankCode = "";
            embedData = embedData.substring(0,embedData.length() - 1) + ",\"bankgroup\": \"ATM\"}";
        }

        log.info("Params: "
                + appUser + "|"
                + appTime + "|"
                + amount + "|"
                + appTransId + "|"
                + embedData + "|"
                + item + "|"
                + description + "|"
                + bankCode
        );

        String data = appId + "|"
                + appTransId
                + "|" + appUser
                + "|" + amount
                + "|" + appTime
                + "|" + embedData
                + "|" + item
                ;

        String mac = HMACUtil.HMacHexStringEncode(HMACUtil.HMACSHA256, key1, data);
        log.info("Mac SHA256: "  + mac);

        CreateOrderRequest createOrderRequest = new CreateOrderRequest(
                appId,
                appUser,
                appTransId,
                appTime,
                amount,
                key1,
                key2,
                orderType,
                "",
                description,
                callbackCreateOrderDomain,
                "",
                item,
                embedData,
                "VND",
                bankCode,
                "0925226173",
                "",
                "",
                "",
//                "",
                "currency=VND&phone=0925226173",
                mac
        );
        String json = GsonUtils.toJsonString(createOrderRequest);
        log.info("JSON: " + json);
        CreateOrderResponse createOrderResponse = new CreateOrderResponse();
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(createOrderDomain);
            StringEntity entity = new StringEntity(json);
            entity.setContentType("application/json");
            httpPost.setEntity(entity);
            log.info("Entity params: " + entity);
            CloseableHttpResponse response = httpClient.execute(httpPost);
            log.info("HttpPost: " + httpPost.getEntity());
            HttpEntity entity1 = response.getEntity();
            String result = EntityUtils.toString(entity1);

            createOrderResponse = GsonUtils.fromJsonString(result, CreateOrderResponse.class);
            log.info("Respone: " + result);
            log.info("Status code: " + String.valueOf(response.getStatusLine().getStatusCode()));
            httpClient.close();
        } catch (Exception ex){
            log.info("Exception create order: " + ex.getMessage());
        }
        return ResponseEntity.ok(createOrderResponse);
    }
    @PostMapping("callback-create-order")
    public ResponseEntity<?> callbackCreateOrder(@RequestBody CallbackCreateOrderRequest callbackCreateOrderRequest) {
        CallbackCreateOrderResponse callbackCreateOrderResponse = new CallbackCreateOrderResponse();
        try {
            String data = callbackCreateOrderRequest.getData();
            String reqMac = callbackCreateOrderRequest.getMac();
            String type = callbackCreateOrderRequest.getType();

            String mac = HMACUtil.HMacHexStringEncode(HMACUtil.HMACSHA256, key2, data);


            if (!reqMac.equals(mac)) {
                log.info("Callback create order mac not equal:");
                callbackCreateOrderResponse.setReturn_code(-1);
                callbackCreateOrderResponse.setReturn_message("mac not equal");
            } else {
                log.info("Callback create order success: " + codeOrders);
                UpdatePaymentStatusRequest updatePaymentStatusRequest = new UpdatePaymentStatusRequest();
                List<UpdatePamentStatusDataRequest> updatePamentStatusDataRequestList = new ArrayList<>();

                for (long item:codeOrders) {
                    log.info("Code orders: " + item);
                    UpdatePamentStatusDataRequest updatePamentStatusDataRequest = new UpdatePamentStatusDataRequest();
                    updatePamentStatusDataRequest.setOrderId(item);
                    updatePamentStatusDataRequest.setPaymentStatus(2);
                    updatePamentStatusDataRequestList.add(updatePamentStatusDataRequest);
                }
                updatePaymentStatusRequest.setData(updatePamentStatusDataRequestList);
                String json = GsonUtils.toJsonString(updatePaymentStatusRequest);
                log.info("JSON : " + json);
                CloseableHttpClient httpClient = HttpClients.createDefault();
                HttpPost httpPost = new HttpPost(updatePaymentStatusDomain);
                StringEntity entity = new StringEntity(json);
                entity.setContentType("application/json");
                httpPost.setEntity(entity);
                log.info("Entity params: " + entity);
                CloseableHttpResponse response = httpClient.execute(httpPost);
                log.info("HttpPost: " + httpPost.getEntity());
                HttpEntity entity1 = response.getEntity();
                String result = EntityUtils.toString(entity1);
                log.info("Respone: " + result);
                log.info("Status code: " + String.valueOf(response.getStatusLine().getStatusCode()));
                httpClient.close();
                callbackCreateOrderResponse.setReturn_code(1);
                callbackCreateOrderResponse.setReturn_message("success");
            }
        } catch (Exception ex) {
            try {
                UpdatePaymentStatusRequest updatePaymentStatusRequest = new UpdatePaymentStatusRequest();
                List<UpdatePamentStatusDataRequest> updatePamentStatusDataRequestList = new ArrayList<>();
                log.info("Callback create order fail: " + codeOrders);
                for (long item:codeOrders) {
                    log.info("Code orders: " + item);
                    UpdatePamentStatusDataRequest updatePamentStatusDataRequest = new UpdatePamentStatusDataRequest();
                    updatePamentStatusDataRequest.setOrderId(item);
                    updatePamentStatusDataRequest.setPaymentStatus(1);
                    updatePamentStatusDataRequestList.add(updatePamentStatusDataRequest);
                }
                updatePaymentStatusRequest.setData(updatePamentStatusDataRequestList);
                String json = GsonUtils.toJsonString(updatePaymentStatusRequest);
                log.info("JSON : " + json);
                CloseableHttpClient httpClient = HttpClients.createDefault();
                HttpPost httpPost = new HttpPost(updatePaymentStatusDomain);
                StringEntity entity = new StringEntity(json);
                entity.setContentType("application/json");
                httpPost.setEntity(entity);
                log.info("Entity params: " + entity);
                CloseableHttpResponse response = httpClient.execute(httpPost);
                log.info("HttpPost: " + httpPost.getEntity());
                HttpEntity entity1 = response.getEntity();
                String result = EntityUtils.toString(entity1);
                log.info("Respone: " + result);
                log.info("Status code: " + String.valueOf(response.getStatusLine().getStatusCode()));
                httpClient.close();
            } catch (Exception e) {
                log.info("Call api update status failed" + e.getMessage());
            }
            callbackCreateOrderResponse.setReturn_code(0);
            callbackCreateOrderResponse.setReturn_message(ex.getMessage());
            log.info("Exception: " + ex.getMessage());
        }
        log.info("Callback create order: " + callbackCreateOrderResponse.getReturn_code() + callbackCreateOrderResponse.getReturn_message());
        return ResponseEntity.ok(callbackCreateOrderResponse);
    }

    @PostMapping("redirect-url")
    public ResponseEntity<?> redirectUrl(@RequestBody RedirectUrlRequest redirectUrlRequest) {
        RedirectUrlResponse redirectUrlResponse = new RedirectUrlResponse();
        try {
            int appId = redirectUrlRequest.getAppid();
            String appTransId = redirectUrlRequest.getApptransid();
            int pmcId = redirectUrlRequest.getPmcid();
            String backCode = redirectUrlRequest.getBankcode();
            long amount = redirectUrlRequest.getAmount();
            long discountAmount = redirectUrlRequest.getDiscountamount();
            int status = redirectUrlRequest.getStatus();
            String checkSumReq = redirectUrlRequest.getChecksum();

            String checkSumData = appId + "|" + appTransId + "|" +
                    pmcId + "|" + backCode + "|" + amount + "|" +
                    discountAmount + "|" + status;
            String checkSum = HMACUtil.HMacHexStringEncode(HMACUtil.HMACSHA256, key2, checkSumData);


            if (!checkSumReq.equals(checkSum)) {
                log.info("Redirect url check sum not equal");
                redirectUrlResponse.setReturn_code(400);
                redirectUrlResponse.setReturn_message("check sum not equal");
            } else {
                log.info("Redirect url check sum equal");
//                log.info("Callback create order success: " + codeOrders);
//                UpdatePaymentStatusRequest updatePaymentStatusRequest = new UpdatePaymentStatusRequest();
//                List<UpdatePamentStatusDataRequest> updatePamentStatusDataRequestList = new ArrayList<>();
//
//                for (long item:codeOrders) {
//                    log.info("Code orders: " + item);
//                    UpdatePamentStatusDataRequest updatePamentStatusDataRequest = new UpdatePamentStatusDataRequest();
//                    updatePamentStatusDataRequest.setOrderId(item);
//                    updatePamentStatusDataRequest.setPaymentStatus(2);
//                    updatePamentStatusDataRequestList.add(updatePamentStatusDataRequest);
//                }
//                updatePaymentStatusRequest.setData(updatePamentStatusDataRequestList);
//                String json = GsonUtils.toJsonString(updatePaymentStatusRequest);
//                log.info("JSON : " + json);
//                CloseableHttpClient httpClient = HttpClients.createDefault();
//                HttpPost httpPost = new HttpPost(updatePaymentStatusDomain);
//                StringEntity entity = new StringEntity(json);
//                entity.setContentType("application/json");
//                httpPost.setEntity(entity);
//                log.info("Entity params: " + entity);
//                CloseableHttpResponse response = httpClient.execute(httpPost);
//                log.info("HttpPost: " + httpPost.getEntity());
//                HttpEntity entity1 = response.getEntity();
//                String result = EntityUtils.toString(entity1);
//                log.info("Respone: " + result);
//                log.info("Status code: " + String.valueOf(response.getStatusLine().getStatusCode()));
//                httpClient.close();
                redirectUrlResponse.setReturn_code(200);
                redirectUrlResponse.setReturn_message("success");
            }
        } catch (Exception ex) {
//            try {
//                UpdatePaymentStatusRequest updatePaymentStatusRequest = new UpdatePaymentStatusRequest();
//                List<UpdatePamentStatusDataRequest> updatePamentStatusDataRequestList = new ArrayList<>();
//                log.info("Callback create order fail: " + codeOrders);
//                for (long item:codeOrders) {
//                    log.info("Code orders: " + item);
//                    UpdatePamentStatusDataRequest updatePamentStatusDataRequest = new UpdatePamentStatusDataRequest();
//                    updatePamentStatusDataRequest.setOrderId(item);
//                    updatePamentStatusDataRequest.setPaymentStatus(1);
//                    updatePamentStatusDataRequestList.add(updatePamentStatusDataRequest);
//                }
//                updatePaymentStatusRequest.setData(updatePamentStatusDataRequestList);
//                String json = GsonUtils.toJsonString(updatePaymentStatusRequest);
//                log.info("JSON : " + json);
//                CloseableHttpClient httpClient = HttpClients.createDefault();
//                HttpPost httpPost = new HttpPost(updatePaymentStatusDomain);
//                StringEntity entity = new StringEntity(json);
//                entity.setContentType("application/json");
//                httpPost.setEntity(entity);
//                log.info("Entity params: " + entity);
//                CloseableHttpResponse response = httpClient.execute(httpPost);
//                log.info("HttpPost: " + httpPost.getEntity());
//                HttpEntity entity1 = response.getEntity();
//                String result = EntityUtils.toString(entity1);
//                log.info("Respone: " + result);
//                log.info("Status code: " + String.valueOf(response.getStatusLine().getStatusCode()));
//                httpClient.close();
//            } catch (Exception e) {
//                log.info("Call api update status failed" + e.getMessage());
//            }
//            callbackCreateOrderResponse.setReturn_code(0);
//            callbackCreateOrderResponse.setReturn_message(ex.getMessage());
            log.info("Exception: " + ex.getMessage());
        }
        log.info("Redirect url" + redirectUrlRequest.getStatus());
        return ResponseEntity.ok(redirectUrlResponse);
    }
}
